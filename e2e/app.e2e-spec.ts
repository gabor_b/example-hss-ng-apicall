import { ExampleHssNgApicallPage } from './app.po';

describe('example-hss-ng-apicall App', () => {
  let page: ExampleHssNgApicallPage;

  beforeEach(() => {
    page = new ExampleHssNgApicallPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
