import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HssModule } from './hss/hss.module';
import { ConfigInterface as HssConfigInterface } from './hss/config';
import { AppComponent } from './app.component';

const hssConfig: HssConfigInterface = {
  apikey: '',
  apiroot: 'https://api.hotspotsystem.com/v2.0'
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HssModule.forRoot(hssConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
