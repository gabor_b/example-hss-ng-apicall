import { Injectable, Inject } from '@angular/core';
import { BaseRequestOptions, RequestOptions } from '@angular/http';

import { ConfigInterface, configToken } from './config';

@Injectable()
export class DefaultRequestOptions extends BaseRequestOptions {

  constructor(
    @Inject(configToken) private config: ConfigInterface,
  ) {
    super();

    this.headers.set('Content-Type', 'application/json');
    this.headers.set('sn-apikey', this.config.apikey);
  }

}

export const requestOptionsProvider = { provide: RequestOptions, useClass: DefaultRequestOptions };