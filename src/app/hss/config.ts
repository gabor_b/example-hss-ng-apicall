import { InjectionToken } from '@angular/core';

export class ConfigInterface {
  apikey: string;
  apiroot: string;
}

export const configToken = new InjectionToken<ConfigInterface>('HssModule.Config');
