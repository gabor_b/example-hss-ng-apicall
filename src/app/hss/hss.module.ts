import { NgModule, ModuleWithProviders, InjectionToken } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { ConfigInterface, configToken } from './config';
import { requestOptionsProvider } from './default-request-options.service';
import { LocationService } from './location.service';

@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  declarations: []
})
export class HssModule {
  static forRoot(providedConfig: ConfigInterface): ModuleWithProviders {
    return {
      ngModule: HssModule,
      providers: [
        {
          provide: configToken,
          useValue: providedConfig
        },
        requestOptionsProvider,
        LocationService
      ]
    };
  }
}
