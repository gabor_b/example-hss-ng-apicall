import { Component, OnInit } from '@angular/core';

import { LocationService } from './hss/location.service';
import { Location } from './hss/location';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  errorMessage: string;
  locations: Location[];

  constructor (private locationService: LocationService) {}

  ngOnInit() {
    this.getLocations();
  }

  getLocations() {
    this.locationService.getLocations()
                     .subscribe(
                       locations => this.locations = locations,
                       error =>  this.errorMessage = <any>error);
  }

}
